Autopoweroff
======================================================================

Autopoweroff if available at it's
[GitHub repository](https://github.com/deragon/autopoweroff).

Until GitLab provides a more standard, easy way to store binaries for
distribution to end users, Autopoweroff remain hosted at GitLab.  This
repository was an attempt to migrate to GitLab, but aborted when it was
discovered that there is no way to provide binaries.
<br/>
<br/>

Hans Deragon, author and custodian of Autopoweroff.

Email:     <hans@deragon.biz><br>
Website:   [www.deragon.biz](http://www.deragon.biz)<br>
